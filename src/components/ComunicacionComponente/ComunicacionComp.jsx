import React from 'react'
import PropTypes from 'prop-types';

export const ComunicacionComp = ({
   title, 
   subtitulo,
   Description, 
  }) => {
  return (
    <div>
        <h1>{ title } </h1>
        <p> { subtitulo } </p>
        <p> { Description }  </p>
    </div>
  )
};

ComunicacionComp.propTypes = {
  title: PropTypes.string.isRequired,
  subtitulo:PropTypes.number,
  Description:PropTypes.string,
};

ComunicacionComp.defaultProps = {
  title:'No hay titulo',
  subtitulo:'no hay sub titulo',
  Description:'El poder de los sayayin',
};
