import React, { useState } from 'react'
import PropTypes from 'prop-types';

export const CounterApp = ({value}) => {
    const [counter, setCounter] = useState(value);
    //funcion anonimo->>> function(event){console.log(event)}
    const handleAdd = ()=>{
        setCounter(counter+1);
    };
    const handleDecrementar = () => {
        setCounter(counter-1);
    }

    const handleReset = () => {
        setCounter(value);
    }
  return (
    <div>
        <h1>CounterApp::: {counter}</h1>
        <h2> { value } </h2>

        <button onClick={handleAdd} >
            Incrementar ++
        </button>
        <button onClick={handleDecrementar} >
            Decrementar --
        </button>
        <button onClick={handleReset}>
            Reset
        </button>
    </div>
  )
}

CounterApp.prototype = {
    value: PropTypes.number.isRequired,
}