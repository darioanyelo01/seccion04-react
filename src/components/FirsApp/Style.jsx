import React from 'react'
import './Style.css';

export const Style = (props) => {
  return (
    <div className='Style'>
        {props.children}
    </div>
  )
}
