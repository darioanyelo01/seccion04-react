
import React from 'react'
import { Style } from './Style';

const ArrayNumero = [1,2,3,4,5,6];

const newMessage = 'Goku';

const ObjectUser = {
    name:"goku",
    age:18,
    description:"Description"
}

export const FirsApp = () => {
 
  return (
    <div>
        
        <Style>
        <h3>El titulo es: {newMessage} </h3>
        <hr/>
        <h3>Los Numeros del Array : {ArrayNumero}</h3>
        <hr/>
        <h3>Los Objetos de Usuario : {`Edad: ${ObjectUser.age} Nombre: ${ObjectUser.name} Description: ${ObjectUser.description}`} </h3>
        <hr/>
        <p>soy un Subtitulo </p>
        <pre>
        {JSON.stringify(ObjectUser, null, 2)}
        </pre>

        </Style>
        
    </div>
  )
}
